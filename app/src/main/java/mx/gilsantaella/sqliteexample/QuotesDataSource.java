package mx.gilsantaella.sqliteexample;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

/**
 * Created by Insofast on 04/11/2015.
 */
public class QuotesDataSource {

    public static final String QUOTES_TABLE_NAME = "Quotes";
    public static final String STRING_TYPE = "text";
    public static final String INT_TYPE = "integer";

    public static class ColumnQuotes{
        public static final String ID_QUOTES = BaseColumns._ID;
        public static final String BODY_QUOTES = "body";
        public static final String AUTHOR_QUOTES = "author";
    }

    public static final String CREATE_QUOTES_SCRIPT =
            "create table " + QUOTES_TABLE_NAME + "(" +
                    ColumnQuotes.ID_QUOTES + " " + INT_TYPE + " primary key autoincrement," +
                    ColumnQuotes.BODY_QUOTES + " " + STRING_TYPE + " not null," +
                    ColumnQuotes.AUTHOR_QUOTES + " " + STRING_TYPE + " not null)";

    public static final String INSERT_QUOTES_SCRIPT =
            "insert into "+QUOTES_TABLE_NAME+" values(" +
                    "null," +
                    "\"El ignorante afirma, el sabio duda y reflexiona\"," +
                    "\"Aristóteles\")," +
                    "(null," +
                    "\"Hay derrotas que tienen mas dignidad que la victoria\"," +
                    "\"Jorge Luis Borges\")," +
                    "(null," +
                    "\"Si buscas resultados distintos, no hagas siempre lo mismo\"," +
                    "\"Albert Einstein\")," +
                    "(null," +
                    "\"Donde mora la libertad, allí está mi patria\"," +
                    "\"Benjamin Franklin\")," +
                    "(null," +
                    "\"Ojo por ojo y todo el mundo acabará ciego\"," +
                    "\"Mahatma Gandhi\")";

    private QuotesReaderDbHelper openHelper;
    private SQLiteDatabase database;

    public QuotesDataSource(Context context) {
        openHelper = new QuotesReaderDbHelper(context);
        database = openHelper.getWritableDatabase();
    }

    public void saveQuoteRow(String body, String author){
        ContentValues values = new ContentValues();

        values.put(ColumnQuotes.BODY_QUOTES, body);
        values.put(ColumnQuotes.AUTHOR_QUOTES, author);
        database.insert(QUOTES_TABLE_NAME, null, values);
    }

    public Cursor getAllQuotes() {
        return database.rawQuery(
                "select * from " + QUOTES_TABLE_NAME, null);
    }

}
